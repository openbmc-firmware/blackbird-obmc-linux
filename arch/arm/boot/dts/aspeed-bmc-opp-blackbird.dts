/dts-v1/;

#include "aspeed-g5.dtsi"
#include <dt-bindings/gpio/aspeed-gpio.h>

/ {
	model = "Blackbird BMC";
	compatible = "rcs,blackbird-bmc", "aspeed,ast2500";

	chosen {
		stdout-path = &uart5;
		bootargs = "console=ttyS4,115200 earlyprintk";
	};

	memory@80000000 {
		reg = <0x80000000 0x20000000>;
	};

	reserved-memory {
		#address-cells = <1>;
		#size-cells = <1>;
		ranges;

		vga_memory: framebuffer@9f000000 {
			no-map;
			reg = <0x9f000000 0x01000000>; /* 16M */
		};

		flash_memory: region@98000000 {
			no-map;
			reg = <0x98000000 0x04000000>; /* 64M */
		};

		coldfire_memory: codefire_memory@9ef00000 {
			reg = <0x9ef00000 0x00100000>;
			no-map;
		};

		gfx_memory: framebuffer {
			size = <0x01000000>;
			alignment = <0x01000000>;
			compatible = "shared-dma-pool";
			reusable;
		};

		video_engine_memory: jpegbuffer {
			size = <0x02000000>;	/* 32M */
			alignment = <0x01000000>;
			compatible = "shared-dma-pool";
			reusable;
		};
	};

	leds {
		compatible = "gpio-leds";

		fault {
			gpios = <&gpio ASPEED_GPIO(N, 2) GPIO_ACTIVE_LOW>;
		};

		identify {
			gpios = <&gpio ASPEED_GPIO(N, 4) GPIO_ACTIVE_HIGH>;
		};

		power {
			gpios = <&gpio ASPEED_GPIO(R, 5) GPIO_ACTIVE_LOW>;
		};

		bmc_ready {
			gpios = <&gpio ASPEED_GPIO(R, 1) GPIO_ACTIVE_LOW>;
		};

		bmc_beep {
			gpios = <&gpio ASPEED_GPIO(N, 7) GPIO_ACTIVE_LOW>;
		};
	};

	fsi: gpio-fsi {
		compatible = "aspeed,ast2500-cf-fsi-master", "fsi-master";
		#address-cells = <2>;
		#size-cells = <0>;
		no-gpio-delays;

		memory-region = <&coldfire_memory>;
		aspeed,sram = <&sram>;
		aspeed,cvic = <&cvic>;

		clock-gpios = <&gpio ASPEED_GPIO(H, 3) GPIO_ACTIVE_HIGH>;
		data-gpios = <&gpio ASPEED_GPIO(H, 2) GPIO_ACTIVE_HIGH>;
		mux-gpios = <&gpio ASPEED_GPIO(A, 6) GPIO_ACTIVE_HIGH>;
		enable-gpios = <&gpio ASPEED_GPIO(D, 0) GPIO_ACTIVE_HIGH>;
		trans-gpios = <&gpio ASPEED_GPIO(H, 1) GPIO_ACTIVE_HIGH>;
	};

	gpio-keys {
		compatible = "gpio-keys";

		checkstop {
			label = "checkstop";
			gpios = <&gpio ASPEED_GPIO(J, 2) GPIO_ACTIVE_LOW>;
			linux,code = <ASPEED_GPIO(J, 2)>;
		};

		id-button {
			label = "id-button";
			gpios = <&gpio ASPEED_GPIO(Q, 7) GPIO_ACTIVE_LOW>;
			linux,code = <ASPEED_GPIO(Q, 7)>;
		};
	};

	iio-hwmon-battery {
		compatible = "iio-hwmon";
		io-channels = <&adc 12>;
	};

};

&pwm_tacho {
	status = "okay";
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_pwm0_default &pinctrl_pwm1_default>;

	/* System fan 1 */
	fan@0 {
		reg = <0x00>;
		aspeed,fan-tach-ch = /bits/ 8 <0x08>;
	};

	/* System fan 2 */
	fan@1 {
		reg = <0x00>;
		aspeed,fan-tach-ch = /bits/ 8 <0x09>;
	};

	/* System fan 3 */
	fan@2 {
		reg = <0x01>;
		aspeed,fan-tach-ch = /bits/ 8 <0x0a>;
	};
};

&fmc {
	status = "okay";
	flash@0 {
		status = "okay";
		m25p,fast-read;
		label = "bmc";
		spi-max-frequency = <25000000>;
#include "openbmc-flash-layout.dtsi"
	};
};

&spi1 {
	status = "okay";
	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_spi1_default>;

	flash@0 {
		status = "okay";
		m25p,fast-read;
		label = "pnor";
		spi-max-frequency = <100000000>;
	};
};

&lpc_ctrl {
	status = "okay";
	memory-region = <&flash_memory>;
	flash = <&spi1>;
};

&lpc_snoop {
	status = "okay";
	snoop-ports = <0x81 0x82>;
};

&mbox {
	status = "okay";
};

&uart1 {
	/* Rear RS-232 connector */
	status = "okay";

	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_txd1_default
			&pinctrl_rxd1_default
			&pinctrl_nrts1_default
			&pinctrl_ndtr1_default
			&pinctrl_ndsr1_default
			&pinctrl_ncts1_default
			&pinctrl_ndcd1_default
			&pinctrl_nri1_default>;
};

&uart5 {
	status = "okay";
};

&mac0 {
	status = "okay";

	use-ncsi;

	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_rmii1_default>;
};

&i2c0 {
	/* CPU0 */
	status = "okay";

	/* SEEPROM bank 0 */
	eeprom@54 {
		compatible = "atmel,24c256";
		reg = <0x54>;
		pagesize = <64>;
	};
	/* SEEPROM bank 1 */
	eeprom@55 {
		compatible = "atmel,24c256";
		reg = <0x55>;
		pagesize = <64>;
	};
	/* SEEPROM bank 2 */
	eeprom@56 {
		compatible = "atmel,24c256";
		reg = <0x56>;
		pagesize = <64>;
	};
	/* SEEPROM bank 3 */
	eeprom@57 {
		compatible = "atmel,24c256";
		reg = <0x57>;
		pagesize = <64>;
	};
};

&i2c1 {
	status = "disabled";
};

&i2c2 {
	/* FSI Mux */

	status = "okay";
};

&i2c3 {
	status = "okay";

	/* Clock chip */
};

&i2c4 {
	status = "okay";

	/* Voltage regulators:
	 *  CPU0 VDD/VCS
	 *  CPU0 VDN/VIO
	 *  CPU0 VDDR A
	 */
};

&i2c5 {
	status = "okay";

	/* HDMI transmitter */
};

&i2c6 {
	/* PCIe slot 1 (x8) */
	status = "okay";
};

&i2c7 {
	/* PCIe slot 2 (x16) */
	status = "okay";
};

&i2c8 {
	status = "disabled";
};

&i2c9 {
	status = "disabled";
};

&i2c10 {
	status = "disabled";
};

&i2c11 {
	status = "okay";

	rtc@32 {
		compatible = "epson,rx8900";
		reg = <0x32>;
	};

	/* TPM */
};

&i2c12 {
	status = "okay";
	bus-frequency = <10000>;

	/* FPGA */
	/* Power Supply */
	/* Temp Sensor */

	w83773g@4c {
		compatible = "nuvoton,w83773g";
		reg = <0x4c>;
	};
};

&i2c13 {
	status = "disabled";
};

&gpio {
    nic_func_mode0 {
		gpio-hog;
		gpios = <ASPEED_GPIO(D, 3) GPIO_ACTIVE_HIGH>;
		output-low;
		line-name = "nic_func_mode0";
    };
    nic_func_mode1 {
		gpio-hog;
		gpios = <ASPEED_GPIO(D, 4) GPIO_ACTIVE_HIGH>;
		output-low;
		line-name = "nic_func_mode1";
    };
};

&vuart {
	status = "okay";
};

&gfx {
	status = "okay";
	output-dvo;

	pinctrl-names = "default";
	pinctrl-0 = <&pinctrl_vpo_default>;
};

&pinctrl {
	aspeed,external-nodes = <&gfx &lhc>;
};

&ibt {
	status = "okay";
};

&vhub {
	status = "okay";
};

&adc {
	status = "okay";
};

&gfx {
	status = "okay";
	memory-region = <&gfx_memory>;
};

&video {
	status = "okay";
	memory-region = <&video_engine_memory>;
};

#include "ibm-power9-dual.dtsi"
